#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Adafruit_IO import Client, Feed, Group
try:
    homedir = os.path.expanduser('~/')
    keypath = homedir+'.aiokey'
    aiokey = open(keypath, 'r').readline()
    aiokey = aiokey.strip()
    userpath= homedir+'.aiouser'
    user = open(userpath, 'r').readline()
    user = user.strip()
except:
    print("Adafruit-IO key file missing, exiting")
    exit(1)


aio = Client(user, aiokey)

feeds = aio.feeds()
print("Feeds: {}".format(feeds))

group_key= 'enviropi'

for i in ['Enviro+ Nomad pressure','Enviro+ Nomad light',
         'Enviro+ Nomad oxidising', 'Enviro+ Nomad reducing',
         'Enviro+ Nomad nh3', 'Enviro+ Nomad pm1', 
         'Enviro+ Nomad pm25', 'Enviro+ Nomad pm10']:
    feed = Feed(name=i)
    response = aio.create_feed(feed, group_key)
    print("New feed: ", response)
