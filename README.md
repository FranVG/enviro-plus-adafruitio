# Enviro+ plus AdafruitIO
Read data from Enviro+ and push it to AdafruitIO using the AdafruitIO API.

## Installation
Clone this repository and create the secrets file .aiokey

## Usage
Can be used a systemd service or launched manually from the command line.
Example files are provided.

## Roadmap
There is no roadmap, but I will probably add noise measurements in the future.
## Contributing
All contributions are welcome, not only code.
Find me in mastodon @alter_unicorn@masto.bike

## Author
Me, based on the examples provided by Pimironi. AdafruitIO and typer.

## License
This licenced under the Unlicence licence.
https://unlicense.org