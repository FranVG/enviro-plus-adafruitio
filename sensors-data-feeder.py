#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import sys
import os
import typer


try:
    homedir = os.path.expanduser('~/')
    keypath = homedir+'.aiokey'
    aiokey = open(keypath, 'r').readline()
    aiokey = aiokey.strip()
    userpath= homedir+'.aiouser'
    user = open(userpath, 'r').readline()
    user = user.strip()
    feedkeybase = 'enviropi.enviro-plus-nomad'  # modify this value to fit your environment
except:
    print("Adafruit-IO key file missing, exiting")
    exit(1)


try:
    from ltr559 import LTR559
    ltr559 = LTR559()
except ImportError:
    import ltr559
from bme280 import BME280
from pms5003 import PMS5003, ReadTimeoutError as pmsReadTimeoutError, SerialTimeoutError
from enviroplus import gas

from Adafruit_IO import Client
aio = Client(user, aiokey)

# BME280 temperature/pressure/humidity sensor
bme280 = BME280()

# PMS5003 particulate sensor
pms5003 = PMS5003()
time.sleep(1.0)

app = typer.Typer()

def measure():
    pid = str(os.getpid())
    open('/tmp/aios.pid', 'w').write(pid)
    while True:
        try:
            proximity   = ltr559.get_proximity()
            pressure    = bme280.get_pressure()
            humidity    = bme280.get_humidity()
            temperature = bme280.get_temperature()
            lux = ltr559.get_lux()
            gases = gas.read_all()
            try:
                particules = pms5003.read()
            except (SerialTimeoutError, pmsReadTimeoutError):
                print("Failed to read PMS5003")

            print("proximity    :     {}".format(proximity))
            print("presssure hPa:     {}".format(pressure))
            print("humidity %   :     {}".format(humidity))
            print("temperature C:     {}".format(temperature))
            print("light Lux    :     {}".format(lux))
            print("oxidising    :     {}".format(gases.oxidising))
            print("reducing     :     {}".format(gases.reducing))
            print("nh3          :     {}".format(gases.nh3))
            print("pm1          :     {}".format(particules.pm_ug_per_m3(1.0)))
            print("pm25         :     {}".format(particules.pm_ug_per_m3(2.5)))
            print("pm10         :     {}".format(particules.pm_ug_per_m3(10)))
            print()

            #feeds are created with feedbuilder.py
            aio.send(feedkeybase, humidity )
            aio.send(feedkeybase+'-temp', temperature )
            aio.send(feedkeybase+'-pressure', pressure)
            aio.send(feedkeybase+'-light', lux)
            aio.send(feedkeybase+'-oxidising', gases.oxidising)
            aio.send(feedkeybase+'-reducing', gases.reducing)
            aio.send(feedkeybase+'-nh3', gases.nh3)
            aio.send(feedkeybase+'-pm1', particules.pm_ug_per_m3(1.0))
            aio.send(feedkeybase+'-pm25', particules.pm_ug_per_m3(2.5))
            aio.send(feedkeybase+'-pm10', particules.pm_ug_per_m3(10))
            time.sleep(60) # one update per minute
        except KeyboardInterrupt:
            if os.path.exists('/tmp/aios.pid'):
                os.remove('/tmp/aios.pid')
            print("Bye!")
            sys.exit(0)

@app.command()
def start():
    measure()

@app.command()
def stop():
    pid = int(open('/tmp/aios.pid','r').readline())
    os.remove('/tmp/aios.pid')
    print("killing pid: {}".format(pid))
    os.kill(pid, 9)

@app.command()
def restart():
    print("Restart called, stopping and starting service")
    if os.path.exists('/tmp/aios.pid'):
        stop()
    time.sleep(1)
    start()

if __name__ == "__main__":
    app()


# from enviroplus.noise import Noise
# noise = Noise()
# amps = noise.get_amplitudes_at_frequency_ranges([(50,499),(500,999),(1000,1500)])
# print(amps)
